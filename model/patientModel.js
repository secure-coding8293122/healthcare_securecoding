// model.js

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const SALT_WORK_FACTOR = 10;

const patientSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    confirmpassword: {
        type: String,
        required: true
    }
});

// Password hashing middleware
patientSchema.pre('save', function(next) {
    const patient = this;

    // Only hash the password if it's modified or new
    if (!patient.isModified('password')) return next();

    // Generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // Hash the password using the generated salt
        bcrypt.hash(patient.password, salt, function(err, hash) {
            if (err) return next(err);

            // Override the cleartext password with the hashed one
            patient.password = hash;
            next();
        });
    });
});

// Password verification method
patientSchema.methods.comparePassword = function(candidatePassword, callback) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return callback(err);
        callback(null, isMatch);
    });
};

const Patient = mongoose.model("login", patientSchema);

module.exports = Patient;
