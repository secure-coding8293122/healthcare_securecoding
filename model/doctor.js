const mongoose = require('mongoose');

const doctorSchema = new mongoose.Schema({
  first_name: String,
  last_name: String,
  department: String,
  address: String,
  phone: String,
  secret: Number,
  image: String

});

const Doctor = mongoose.model('Doctor', doctorSchema);

module.exports = Doctor;
