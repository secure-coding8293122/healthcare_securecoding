// model.js

const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    fullName: String,
    dob: String,
    email: String,
    phone: String,
    reason: String,
    preferredDate: String,
    preferredTime: String,
    notapproved: String,
    submissionTimestamp: Date

});

const Users = mongoose.model("data", userSchema);
module.exports = Users;
