const express = require('express');
const path = require('path');
const Users = require('../healthcare_securecoding/model/formModel');
const base64 = require('base-64');

const router = express.Router();

router.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'));
});

const escapeHtml = (unsafe) => {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
};

router.post('/post', async (req, res) => {
    try {
        const { fullName, dob, email, phone, reason, preferredDate, preferredTime, notapproved, submissionTimestamp } = req.body;
        if (!fullName || !dob || !email || !phone || !reason || !preferredDate || !preferredTime || !notapproved || !submissionTimestamp) {
            const errorMessage = "All fields are required. Please fill in all details.";
            const sanitizedErrorMessage = escapeHtml(errorMessage);
            return res.status(400).send(`
                <script>
                    alert("${sanitizedErrorMessage}");
                    window.location.href = './patient/form.html';
                </script>
            `);
        }
        
        // Encode form data with Base64
        const encodedFullName = base64.encode(fullName);
        const encodedDob = base64.encode(dob);
        const encodedEmail = base64.encode(email);
        const encodedPhone = base64.encode(phone);
        const encodedReason = base64.encode(reason);
        const encodedPreferredDate = base64.encode(preferredDate);
        const encodedPreferredTime = base64.encode(preferredTime);

        // Store form data in session
        req.session.formData = {
            fullName: encodedFullName,
            dob: encodedDob,
            email: encodedEmail,
            phone: encodedPhone,
            reason: encodedReason,
            preferredDate: encodedPreferredDate,
            preferredTime: encodedPreferredTime,
            notapproved, // Not encoding notapproved
            submissionTimestamp // Storing timestamp as is

        };
        
        // Create a new user with encoded data
        const user = new Users({
            fullName: encodedFullName,
            dob: encodedDob,
            email: encodedEmail,
            phone: encodedPhone,
            reason: encodedReason,
            preferredDate: encodedPreferredDate,
            preferredTime: encodedPreferredTime,
            notapproved, // Not encoding notapproved
            submissionTimestamp: new Date(submissionTimestamp) // Convert to Date object

        });
        
        // Save the user to the database
        await user.save();
        
        // Redirect to form with success message
        const successMessage = "Form submitted successfully";
        res.send(`
            <script>
                alert("${escapeHtml(successMessage)}");
                window.location.href = './patient/form.html';
            </script>
        `);
    } catch (error) {
        console.error(error);
        const errorMessage = "Internal Server Error";
        const sanitizedErrorMessage = escapeHtml(errorMessage);
        res.status(500).send(sanitizedErrorMessage);
    }
});

// Route for fetching appointment data
router.get('/appointments', async (req, res) => {
    try {
        const appointments = await Users.find({});
        
        // Decode appointment data before sending
        const decodedAppointments = appointments.map(appointment => {
            return {
                _id: appointment._id,
                __v: appointment.__v,
                fullName: base64.decode(appointment.fullName),
                dob: base64.decode(appointment.dob),
                email: base64.decode(appointment.email),
                phone: base64.decode(appointment.phone),
                reason: base64.decode(appointment.reason),
                preferredDate: base64.decode(appointment.preferredDate),
                preferredTime: base64.decode(appointment.preferredTime),
                notapproved: appointment.notapproved // Keep notapproved plain
            };
        });
        
        res.json(decodedAppointments);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
});


router.post('/approve-appointment/:appointmentId', async (req, res) => {
    try {
        const appointmentId = req.params.appointmentId;

        // Check if appointmentId is undefined
        if (!appointmentId) {
            return res.status(400).send(`
                <script>
                    alert("Invalid appointment ID");
                    window.location.href = '/';
                </script>
            `);
        }

        // Update the appointment status to "Approved" in the database
        const updatedAppointment = await Users.findByIdAndUpdate(
            appointmentId, 
            { notapproved: 'approved' }, 
            { new: true } // Return the updated document
        );

        if (!updatedAppointment) {
            // If the appointment with the given ID is not found
            return res.status(404).send(`
                <script>
                    alert("Appointment not found");
                    window.location.href = '/';
                </script>
            `);
        }

        // Send a response that includes a script to show an alert
        res.send(`
            <script>
                alert("Appointment approved successfully");
                window.location.href = '/';
            </script>
        `);
    } catch (error) {
        console.error('Error approving appointment:', error);
        res.status(500).send(`
            <script>
                alert("Failed to approve appointment: ${error.message}");
            </script>
        `);
    }
});


// Route to fetch total number of appointments
router.get('/total-appointments', async (req, res) => {
    try {
        const totalAppointments = await Users.countDocuments();
        res.json({ totalAppointments });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
});

module.exports = router;
