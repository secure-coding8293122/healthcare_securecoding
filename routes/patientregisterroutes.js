// routes.js

const express = require('express');
const path = require('path');
const patient = require('../healthcare_securecoding/model/patientModel');

const Patientrouter = express.Router();

Patientrouter.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'));
});

Patientrouter.post('/register', async (req, res) => {
    try {
        const { username, password, confirmpassword } = req.body;
        
        // Validate request data
        if (!username || !password || !confirmpassword) {
            return res.send(`
                <script>
                    alert("Username, password, and confirm password are required.");
                    window.history.back();
                </script>
            `);
        }

        // Check if passwords match
        if (password !== confirmpassword) {
            return res.send(`
                <script>
                    alert("Passwords do not match.");
                    window.history.back();
                </script>
            `);
        }

        // Create a new patient instance
        const user = new patient({
            username,
            password,
            confirmpassword
        });

        // Save the patient to the database
        await user.save();
        console.log("Patient registered:", user);
        res.send(`
            <script>
                alert("Patient Registered successfully");
                window.location.href = './patient/patientlogin.html';
            </script>
        `);
    } catch (error) {
        console.error("Error registering patient:", error);
        res.status(500).send(`
            <script>
                alert("Internal Server Error");
                window.history.back();
            </script>
        `);
    }
});

module.exports = Patientrouter;
