const express = require('express');
const DoctorRoute = express.Router();
const path = require('path');
const Doctor = require('../healthcare_securecoding/model/doctor');
const winston = require('winston');

// Configure Winston
const logger = winston.createLogger({
    transports: [
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.Console({ format: winston.format.simple() })
    ]
});

DoctorRoute.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'));
});

// Route to add a new doctor
DoctorRoute.post('/add', async (req, res) => {
    try {
        const { first_name, last_name, department, address, phone, secret, image } = req.body;

       // Validate request data
        if (!first_name || !last_name || !department || !address   ) {
        return res.send(`
            <script>
                alert("All fields are required.");
                window.location.href = './html/doctorregister.html';
            </script>
        `);
        }

        // Validate phone number (should contain exactly 8 digits)
        if (!/^\d{8}$/.test(phone)) {
        return res.send(`
            <script>
                alert("Phone number must contain exactly 8 digits.");
                window.location.href = './html/doctorregister.html';
            </script>
        `);
        }

        // Validate secret number (should start with '1221' and have 4 more digits)
        if (!/^1221\d{4}$/.test(secret)) {
        return res.send(`
            <script>
                alert("Secret number must start with '1221' and have 4 more digits.");
                window.location.href = './html/doctorregister.html';
            </script>
        `);
        }       // Create a new doctor document
        const newDoctor = new Doctor({
            first_name,
            last_name,
            department,
            address,
            phone,
            secret,
            image,
        });
         // Set doctor data in session
         req.session.doctor = {
            first_name,
            last_name,
            department,
            address,
            phone,
            secret,
            image,
        };

        // Save the new doctor to the database
        await newDoctor.save();

        // Log success message
        logger.info('Doctor added successfully');

        // Alert success message
        res.send(`
            <script>
                alert("Doctor added successfully");
                window.location.href = './html/doctorregister.html';
            </script>
        `);
    } catch (error) {
        logger.error(error);
        res.status(500).send("Internal Server Error");
    }
});

// Route for fetching appointment data
DoctorRoute.get('/doctorlisted', async (req, res) => {
    try {
        // Fetch appointment data from MongoDB
        const doctorlisted = await Doctor.find({});
        res.json(doctorlisted); // Send the fetched appointments as a JSON response
    } catch (error) {
        logger.error(error);
        res.status(500).json({ message: 'Internal server error' }); // Handle errors
    }
});

// Route to delete a doctor
DoctorRoute.delete('/delete/:id', async (req, res) => {
    try {
        const { id } = req.params;

        // Delete the doctor with the given ID from the database
        await Doctor.findByIdAndDelete(id);

        // Log success message
        logger.info('Doctor deleted successfully');

        res.send(`
            <script>
                alert("Doctor deleted successfully");
                window.location.href = '/';
            </script>
        `);
    } catch (error) {
        logger.error(error);
        res.status(500).send("Failed to delete doctor");
    }
});

// Route to fetch total number of appointments
DoctorRoute.get('/total-doctors', async (req, res) => {
    try {
        const totalDoctors = await Doctor.countDocuments();
        res.json({ totalDoctors });
    } catch (error) {
        logger.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
});

module.exports = DoctorRoute;
