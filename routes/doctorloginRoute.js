const express = require('express');
const Doctor = require('../healthcare_securecoding/model/doctor');
const winston = require('winston');
const rateLimit = require("express-rate-limit");

const router = express.Router();
// Configure Winston logger
const logger = winston.createLogger({
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: 'error.log', level: 'error' })
    ]
});
// Define the rate limiter
const loginLimiter = rateLimit({
    windowMs: 30 * 60 * 1000, // 30 minutes
    max: 2, // Max 2 attempts
    message: `
        <script>
            alert("Too many login attempts with incorrect credentials, please try again after 30 minutes");
            window.location.href = './doctor/doctorlogin.html';
        </script>
    `
});

// Apply the rate limiter to all requests to the '/doctorlogin' route
router.post('/doctorlogin', loginLimiter, async (req, res) => {
    try {
        const { first_name, last_name, secret } = req.body;

        // Check if doctor exists in the database
        const doctor = await Doctor.findOne({ first_name, last_name });

        // If doctor is not found
        if (!doctor) {
            logger.error('Doctor not found:', { first_name, last_name });
            return res.send(`
                <script>
                    alert("Invalid credentials");
                    window.location.href = './doctor/doctorlogin.html';
                </script>
            `);
        }

        // Check if the entered secret code matches the one stored in the database
        if (secret.toString() !== doctor.secret.toString()) {
            logger.error('Invalid secret code');
            return res.send(`
                <script>
                    alert("Invalid secret code");
                    window.location.href = './doctor/doctorlogin.html';
                </script>
            `);
        }

        // Set doctor data in session
        req.session.doctor = {
            first_name: doctor.first_name,
            last_name: doctor.last_names
        };

        res.redirect('./doctor/viewappointment.html');

    } catch (error) {
        logger.error('Error:', error);
        res.send(`
            <script>
                alert("Internal server error");
                window.location.href = './doctor/doctorlogin.html';
            </script>
        `);
    }
});

module.exports = router;
