const express = require('express');
const bcrypt = require('bcrypt');
const User = require('../healthcare_securecoding/model/patientModel'); // Import the correct model
const path = require('path'); // Ensure path is imported
const validator = require('validator'); // Import the validator library
const sanitize = require('mongo-sanitize'); // Import mongo-sanitize library
const rateLimit = require('express-rate-limit'); // Import express-rate-limit library

const loginroute = express.Router();

// Define the rate limiter
const loginLimiter = rateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 3, 
    message: `
        <script>
            alert("Too many login attempts from this IP, please try again after 15 minutes");
            window.location.href = './patient/patientlogin.html';
        </script>
    `
});

// Apply the rate limiter to the login route
loginroute.post('/login', loginLimiter, async (req, res) => {
    try {
        let { username, password } = req.body;

        // Validate inputs
        if (!username || !password) {
            return res.status(400).send(`
                <script>
                    alert("Username and password are required.");
                    window.location.href = './patient/patientlogin.html';
                </script>
            `);
        }

        // Sanitize inputs
        username = sanitize(username);
        password = sanitize(password);

        // Validate the username and password using the validator library
        if (!validator.isAlphanumeric(username)) {
            return res.status(400).send(`
                <script>
                    alert("Invalid username format.");
                    window.location.href = './patient/patientlogin.html';
                </script>
            `);
        }

        // Find the user by username in a safe way
        const user = await User.findOne({ username });
        if (!user) {
            return res.status(401).send(`
                <script>
                    alert("Invalid username.");
                    window.location.href = './patient/patientlogin.html';
                </script>
            `);
        }

        // Compare passwords
        const passwordMatch = await bcrypt.compare(password, user.password);
        if (!passwordMatch) {
            return res.status(401).send(`
                <script>
                    alert("Invalid password.");
                    window.location.href = '../patient/patientlogin.html';
                </script>
            `);
        }

        // Set user data in session
        req.session.user = {
            username: user.username,
            // Add more user data as needed
        };

        // Redirect to the next page
        res.redirect('../patient/patientauthority.html');
    } catch (error) {
        console.error("Error logging in:", error);
        res.status(500).send("Internal Server Error");
    }
});

module.exports = loginroute;
