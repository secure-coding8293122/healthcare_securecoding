require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const routes = require('./routes/formRoutes');
const loginRoutes = require('./routes/patientregisterroutes');
const patientLogin = require('./routes/patientLoginRoutes');
const doctorregister = require('./routes/registerdoctor');
const doctorLoginRoutes = require('./routes/doctorloginRoute');

const port = 3019;

const app = express();
app.use(express.static(__dirname));
app.use(express.urlencoded({ extended: true }));

// Session middleware
app.use(session({
    secret: 'A7b!2@dC4#fG8&jK9LmP0QrS%tU5wX3Z',
    resave: false,
    saveUninitialized: false,
    store: MongoStore.create({ mongoUrl: 'mongodb+srv://12210008gcit:u5Y4nAD6CF7HtdKa@cluster0.fymblhq.mongodb.net/' }), // Store sessions in MongoDB
    cookie: {
        maxAge: 1000 * 60 * 60 * 3, // Session expiration time (3 hours)
        // Set secure to true if your app is served over HTTPS
        secure: false, // Set to true in production if using HTTPS
        sameSite: true // Set to 'none' in production if using cross-origin requests
    }
}));
// Middleware to log session data
app.use((req, res, next) => {
    console.log('Session data:', req.session);
    next();
});


mongoose.connect('mongodb+srv://12210008gcit:u5Y4nAD6CF7HtdKa@cluster0.fymblhq.mongodb.net/');
const db = mongoose.connection;
db.once('open', () => {
    console.log("MongoDB connected");
});

// Routes
app.use('/', routes);
app.use('/', loginRoutes);
app.use('/', patientLogin);
app.use('/', doctorregister);
app.use('/', doctorLoginRoutes); 

// Server listening
app.listen(port, () => {
    console.log("Server started");
});
